﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mist : MonoBehaviour {

	public Camera camera;

	void OnTriggerEnter (Collider other)
	{
		switch (other.tag) {
		case "HarryPotter":
			camera.transform.Rotate (0, 0, 180);
			break;
		default:
			break;
		};
	}

	void OnTriggerExit (Collider other)
	{
		switch (other.tag) {
		case "HarryPotter":
			camera.transform.Rotate (0, 0, 180);
			break;
		default:
			break;
		};
	}
}
