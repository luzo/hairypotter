﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChooseSfingeOptions : MonoBehaviour
{
	public Dropdown dropdown;
	public GameObject sfingeRiddleCanvas;
	public GameObject sfingeCanvas;
	public GameObject spider;
	public static string sfingeName;
	private static int numberOfGeneratedSpiders = 10;
	private int initialHpSpider = 50;
	public GameObject player;
	public Transform navigationHolder;
	public bool deprecatedAnimations;
	private Vector3 sfingePositon;
	private Animator _wallsAnimator;
	private GameObject[] sfinges;

	public void updateSfingeName (string newSfineName)
	{
		sfingeName = newSfineName;
	}

	public void updateWallsAnimator (Animator wallsAnimator)
	{
		_wallsAnimator = wallsAnimator;
	}

	private void SetCursor (bool visible)
	{
		Cursor.visible = visible;
		Cursor.lockState = visible ? CursorLockMode.None : CursorLockMode.Locked;
	}

	public void doActionOnAnswer ()
	{
		sfingePositon = GameObject.Find (sfingeName).transform.position;
		sfingeCanvas.SetActive (false);
		switch (dropdown.value) {
		case 0: //riddle
			sfingeRiddleCanvas.SetActive (true);
			RandomRiddle riddle = sfingeRiddleCanvas.GetComponent<RandomRiddle> ();
			riddle.updateWallsAnimator(_wallsAnimator);
			break;
		case 1: //fight ->generate spiders 	
			Time.timeScale = 1.0f;
			GenerateSpiders ();
			Invoke ("OpenWay", 1f);
			break;
		default:
			break;
		}
	}

	private void OpenWay() {
		_wallsAnimator.SetTrigger ("riddleCorrect");
	}

	private void GenerateSpiders ()
	{
		DestroySfinge ();
		for (int i = 0; i < numberOfGeneratedSpiders; i++) {
			GameObject instance = Instantiate (spider, sfingePositon, Quaternion.identity);
			EnemyAnimationController animationCtrl = instance.GetComponent<EnemyAnimationController> ();
			animationCtrl.healthPoints = initialHpSpider;
			animationCtrl.player = player;
			animationCtrl.deprecatedAnimations = deprecatedAnimations;

			NavigationAgent navAgent = instance.GetComponent<NavigationAgent> ();
			navAgent.player = player.transform;
			navAgent.navigationPointsHolder = navigationHolder;
		}
	}

	private void DestroySfinge ()
	{		
		sfinges = GameObject.FindGameObjectsWithTag ("Sfinge");
		foreach(GameObject sfinge in sfinges) {
			sfinge.SetActive(false);
		}

		Invoke ("RefreshSfinge", 60f);
	}

	private void RefreshSfinge ()
	{		
		foreach(GameObject sfinge in sfinges) {
			sfinge.SetActive(true);
		}
	}
}
