﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGameScript : MonoBehaviour {


	private bool isEnabled = false;
	public GameObject pauseMenu;
	private bool visibleCursor = false;
	public GameObject sfingeMenu;
	public GameObject sfingeRiddleCanvas;
	public GameObject gameOverMenu;

	void Start() {
		Cursor.visible = false;
	}


	void Update()
	{
		
		visibleCursor = (pauseMenu.active || sfingeMenu.active || sfingeRiddleCanvas.active || gameOverMenu.active);
		if (Cursor.visible != visibleCursor) {
			Cursor.visible = visibleCursor;
			Cursor.lockState = visibleCursor ? CursorLockMode.None : CursorLockMode.Locked;
		}

		if (Input.GetKeyDown(KeyCode.Escape) && !isEnabled && !sfingeMenu.active)
		{
			pauseMenu.SetActive(true);
			isEnabled = true;
			Time.timeScale = 0.0f;
		}

		else if (Input.GetKeyDown(KeyCode.Escape) && isEnabled)
		{
			pauseMenu.SetActive(false);
			isEnabled = false;
			Time.timeScale = 1.0f;

		}

		if (pauseMenu.active || sfingeRiddleCanvas.active || sfingeMenu.active || gameOverMenu.active) {
			HarrySpellCastController.forbiddenSpelling (pauseMenu.tag);
		} 
		else {
			HarrySpellCastController.allowedSpelling (pauseMenu.tag);
		}
	}

	public void ContinueGame(){

		Time.timeScale = 1.0f;
		pauseMenu.SetActive(false);
		isEnabled = false;
	}

}
