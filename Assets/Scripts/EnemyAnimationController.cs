﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAnimationController : MonoBehaviour
{
	public GameObject player;
	public int healthPoints;
	public bool deprecatedAnimations = false;
	public float stoppingNavDistance = 1.7f;
	public int meleeDamage = 6;
	public int rangeDamage = 0;
	public float closeDamageTime = 1f;
	public float closeDamageDelay = 0.25f;
	public float rangeDamageTime = 1f;
	public float rangeDamageDelay = 0.25f;

	private float fireRangeDistance = 10f;
	private bool canAttack = false;
	private Animation animation;
	private Animator animator;
	private NavMeshAgent agent;
	private bool isStunned = false;
	private bool isDead = false;
	private bool wasHit = false;
	private bool isDamageInvoked = false;

	void Start ()
	{
		if (!deprecatedAnimations) {
			animator = GetComponent<Animator> ();

			if (animator == null) {
				animator = GetComponentInChildren<Animator> ();
			}
		} else {
			animation = GetComponent<Animation> ();

			if (animation == null) {
				animation = GetComponentInChildren<Animation> ();
			}

			animation.Play ("walk");
		}

		agent = GetComponent<NavMeshAgent> ();
	}

	void Update ()
	{
		if (!deprecatedAnimations) {
			UpdateWithAnimator ();
		} else {
			UpdateWithDeprecated ();
		}
	}

	void UpdateWithDeprecated ()
	{
		if (agent.enabled && !isStunned && !isDead) {

			float remainingDistance = agent.remainingDistance;

			RaycastHit hit;
			bool isBlockedByWall = false;

			if (Physics.Linecast (transform.position, player.transform.position, out hit)) {
				isBlockedByWall = hit.transform.tag == "Walls" || hit.transform.tag == "DangerousWalls";
			}

			if (!canAttack && (remainingDistance > fireRangeDistance || isBlockedByWall)) {
				animation.Stop ("idle");
				animation.Play ("walk");
			} else if (canAttack && remainingDistance <= stoppingNavDistance) {
				animation.Stop ("walk");
				animation.Stop ("idle");
				animation.Play ("attack");

				if (!isDamageInvoked) {
					isDamageInvoked = true;
					InvokeRepeating ("DoMeleeDamage", closeDamageDelay, closeDamageTime);
				}
			} else if (
				rangeDamage > 0 && //only for creatures that can shoot
				!canAttack &&
				remainingDistance <= fireRangeDistance &&
				remainingDistance > stoppingNavDistance) {
				
				transform.LookAt (player.transform);
				animation.Stop ("walk");
				animation.Stop ("idle");
				animation.Play ("fireAttack");

				if (!isDamageInvoked) {
					Invoke ("WalkAfterFire", 1f);
					isDamageInvoked = true;
					InvokeRepeating ("DoRangeDamage", rangeDamageDelay, rangeDamageTime);
					agent.enabled = false;
				}
			} else {
				animation.Stop ("idle");
				animation.Play ("walk");
			}
		}
	}

	private void WalkAfterFire ()
	{
		animation.Stop ("fireAttack");
		animation.Play ("walk");
		CancelInvoke ();
		Invoke ("ResetDamageInvoke", 0.5f);
		agent.enabled = true;
	}

	private void ResetDamageInvoke ()
	{
		isDamageInvoked = false;
	}

	private void UpdateWithAnimator ()
	{
		NavMeshAgent agent = GetComponent<NavMeshAgent> ();

		if (agent.enabled && !isStunned && !isDead) {

			float remainingDistance = agent.remainingDistance;

			if (!canAttack && remainingDistance > stoppingNavDistance) {
				animator.SetTrigger ("walk");
			} else if (!canAttack && remainingDistance <= stoppingNavDistance) {
				animator.SetTrigger ("idle");
			} else if (canAttack) {
				animator.SetTrigger ("attack");

				if (!isDamageInvoked) {
					isDamageInvoked = true;
					InvokeRepeating ("DoMeleeDamage", closeDamageDelay, closeDamageTime);
				}

			} else if (!canAttack && remainingDistance < fireRangeDistance) { //a zaroven ked sa jedna o skorpiona

				if (!isDamageInvoked) {
					animator.SetTrigger ("fireAttack");
					isDamageInvoked = true;
					InvokeRepeating ("DoRangeDamage", rangeDamageDelay, rangeDamageTime);
				}
			}
		}
	}

	void OnTriggerEnter (Collider other)
	{
		resolveColision (other.tag);
	}

	void OnTriggerExit (Collider other)
	{
		string tag = other.tag;
		if (tag == "HarryPotter") {

			if (!deprecatedAnimations) {
				//animator.SetTrigger ("attack");
			} else {
				animation.Stop ("attack");
			}
			CancelInvoke ();
			isDamageInvoked = false;
			canAttack = false;
		} else if (tag == "Stupefy Spell" || tag == "Impedimenta Spell" || tag == "Expeliarmus Spell" || tag == "AvadaKedavra Spell") {
			wasHit = false;
		}
	}

	public void resolveColision (string tag)
	{
		if (!isDead) {
			switch (tag) {
			case "Stupefy Spell":
				if (!wasHit) {
					ResolveStupefySpellImpact ();
				}
				break;
			case "Impedimenta Spell":
				if (!wasHit) {
					ResolveImpedimentaSpellImpact ();
				}
				break;
			case "Expeliarmus Spell":
				if (!wasHit) {
					ResolveExpelliarmusSpellImpact ();
				}
				break;
			case "AvadaKedavra Spell":
				if (!wasHit) {
					ResolveAvadaKedavraSpellImpact ();
				}
				break;
			case "HarryPotter":
				canAttack = true;
				isDamageInvoked = false;
				CancelInvoke ();
				break;
			}
		}
	}

	private void ResolveStupefySpellImpact ()
	{
		int damage = 10;
		int freezeTime = 3;
		StartCoroutine (ResolveDamage (damage));
		StartCoroutine (ResolveFreezeTime (freezeTime));
		wasHit = true;
	}

	private void ResolveImpedimentaSpellImpact ()
	{
		int freezeTime = 7;
		StartCoroutine (ResolveFreezeTime (freezeTime));
		wasHit = true;
	}

	private void ResolveExpelliarmusSpellImpact ()
	{
		int damage = 20;
		StartCoroutine (ResolveDamage (damage));
		wasHit = true;
	}

	private void ResolveAvadaKedavraSpellImpact ()
	{
		int damage = 35;
		StartCoroutine (ResolveDamage (damage));
		wasHit = true;
	}

	private IEnumerator ResolveDamage (int damage)
	{
		healthPoints -= damage;
		GetComponent<NavigationAgent> ().agent.enabled = false;

		if (deprecatedAnimations) {
			animation.Stop (); 
		}

		if (healthPoints <= 0) {
			isDead = true;

			if (!deprecatedAnimations) {
				animator.SetTrigger ("death");
			} else {
				animation.Play ("death");
			}

			yield return new WaitForSeconds (8);
			Destroy (this.gameObject);
		} else {
			if (!isStunned) {

				if (!deprecatedAnimations) {
					animator.SetTrigger ("damage");
				} else {
					animation.Play ("damage");
				}
				yield return new WaitForSeconds (1);
				if (!isStunned) {
					GetComponent<NavigationAgent> ().agent.enabled = true;
				}
			}
		}
	}

	private IEnumerator ResolveFreezeTime (int seconds)
	{
		if (deprecatedAnimations) {
			animation.Stop (); 
		}
		GetComponent<NavigationAgent> ().agent.enabled = false;
		isStunned = true;
		CancelInvoke ();
		yield return new WaitForSeconds (seconds);
		isStunned = false;
		isDamageInvoked = false;
		GetComponent<NavigationAgent> ().agent.enabled = true;
	}

	private void DoRangeDamage ()
	{
		player.GetComponent<DamageController> ().reduceHealth (rangeDamage);
	}

	private void DoMeleeDamage ()
	{
		player.GetComponent<DamageController> ().reduceHealth (meleeDamage);
	}
}
