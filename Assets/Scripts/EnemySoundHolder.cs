﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemySoundHolder : MonoBehaviour
{
	public AudioClip attackSound;
	public AudioClip damageSound;
	public AudioClip deathSound;

	public void PlayAttackSound ()
	{
		AudioSource.PlayClipAtPoint (attackSound, transform.position);
	}

	public void PlayDamageSound ()
	{
		AudioSource.PlayClipAtPoint (damageSound, transform.position);
	}

	public void PlayDeathSound ()
	{
		AudioSource.PlayClipAtPoint (deathSound, transform.position);
	}
}

