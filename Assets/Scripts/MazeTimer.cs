﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeTimer : MonoBehaviour {

	// Use this for initialization
	void Start () {
		InvokeRepeating ("StartMazeKilling", 3f, 60f);
		InvokeRepeating ("RebuildMaze", 7f, 60f);
	}

	// Update is called once per frame
	void Update () {
		
	}

	void RebuildMaze () {
		var animator = this.GetComponent <Animator> ();
		animator.SetTrigger ("rebuildPath");
	}

	void StartMazeKilling () {
		var animator = this.GetComponent <Animator> ();
		animator.SetTrigger ("startKilling");
	}
}

