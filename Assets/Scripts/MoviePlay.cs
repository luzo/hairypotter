﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MoviePlay : MonoBehaviour {

	public string nextScene;

	// Use this for initialization
	void Start () {

		MovieTexture movie = (MovieTexture)(GetComponent<Renderer> ().material.mainTexture);
		movie.Play();
		AudioSource audio = GetComponent<AudioSource>();
		audio.Play();
		Invoke("LoadNextScene",movie.duration);
	}

	void Update() {

		if (Input.GetKeyDown( KeyCode.Space)) {
			LoadNextScene ();
		}
	}

	void LoadNextScene() {
		
		SceneManager.LoadScene (nextScene);
	}
}
