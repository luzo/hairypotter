﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavigationAgent : MonoBehaviour
{
	public Transform player;
	public NavMeshAgent agent;
	public float minDistance = 3f;
	public Transform navigationPointsHolder;
	List<Transform> navigationPoints;
	Transform choosedPoint;

	void Start ()
	{
		agent = GetComponent<NavMeshAgent> ();
		navigationPoints = new List<Transform> ();

		foreach (Transform child in navigationPointsHolder) {
			navigationPoints.Add (child);
		}

		InvokeRepeating ("ChangeNavigationPoint", 0f, 20f);
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (!gameObject.activeSelf || !agent.enabled || player == null || choosedPoint == null || transform == null) {
			return;
		}
			
		float dist = Vector3.Distance (player.position, transform.position);
		float choosedPointDist = Vector3.Distance (choosedPoint.position, transform.position);

		if (choosedPointDist < 10f) {
			ChangeNavigationPoint ();
		}

		if (dist < 10f) {
			agent.destination = player.position;
		} else if (choosedPoint != null) {
			agent.destination = choosedPoint.position;
		}
	}

	void ChangeNavigationPoint ()
	{
		int randomIndex = Random.Range (0, navigationPoints.Count);
		choosedPoint = navigationPoints [randomIndex];
	}
}
