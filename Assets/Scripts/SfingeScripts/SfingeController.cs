﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SfingeController : MonoBehaviour
{
	public GameObject sfingeCanvas;
	public Transform player;
	private GameObject sfingeOptions;
	public Animator wallsAnimator;

	void Update ()
	{
		Vector3 position = player.position;
		position.y = this.transform.position.y;
		transform.LookAt (position);
	}

	void OnTriggerEnter (Collider other)
	{
		switch (other.tag) {
		case "HarryPotter":
			Time.timeScale = 0.0f;
			sfingeCanvas.SetActive (true);
			ChooseSfingeOptions options = sfingeCanvas.GetComponent<ChooseSfingeOptions> ();
			options.updateSfingeName (this.gameObject.name);
			options.updateWallsAnimator (wallsAnimator);
			break;
		}
	}

	void OnTriggerExit (Collider other)
	{
		switch (other.tag) {
		case "HarryPotter":
			break;
		}
	}
}
