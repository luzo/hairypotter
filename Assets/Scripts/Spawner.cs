﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Spawner : MonoBehaviour
{
	public GameObject spawn;
	public GameObject player;
	public Transform navigationHolder;
	public float timeToSpawn;
	public int initialHP;
	public bool deprecatedAnimations;
	public Slider hearthSlider;

	private List<Transform> spawnPoints;

	// Use this for initialization
	void Start ()
	{
		spawnPoints = new List<Transform> ();

		foreach (Transform spawnpoint in transform) {
			spawnPoints.Add (spawnpoint);
		}

		InvokeRepeating ("Spawn", 5f, timeToSpawn);
	}

	void Spawn ()
	{
		int randomIndex = Random.Range (0, spawnPoints.Count);
		Vector3 spawnPoint = spawnPoints [randomIndex].position;	

		GameObject instance = Instantiate (spawn, spawnPoint, Quaternion.identity);

		EnemyAnimationController animationCtrl = instance.GetComponent<EnemyAnimationController> ();
		animationCtrl.player = player;
		animationCtrl.healthPoints = initialHP;
		animationCtrl.deprecatedAnimations = deprecatedAnimations;

		EnemySpellCastController enemySpellCtrl = instance.GetComponentInChildren<EnemySpellCastController> ();
		if (enemySpellCtrl != null) {
			enemySpellCtrl.player = player.transform;
		}

		NavigationAgent navAgent = instance.GetComponent<NavigationAgent> ();
		navAgent.player = player.transform;
		navAgent.navigationPointsHolder = navigationHolder;
	}
}
