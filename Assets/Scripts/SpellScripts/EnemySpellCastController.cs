﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitalRuby.PyroParticles;
using UnityEngine.UI;
using System;

public class EnemySpellCastController : MonoBehaviour
{
	public GameObject spellProjectilePrefab;
	public Transform player;
	public Color spellColor;
	public String enemyRangeAttackTag;

	private GameObject spellProjectile;
	private FireBaseScript spellProjectileScript;

	public void LaunchSpellAttack ()
	{
		Vector3 pos;
		float yRot = transform.rotation.eulerAngles.y;
		Vector3 forwardY = Quaternion.Euler (0.0f, yRot, 0.0f) * Vector3.forward;
		Vector3 forward = transform.forward;
		Vector3 up = transform.up;
		Quaternion rotation = Quaternion.identity;
		spellProjectile = GameObject.Instantiate (spellProjectilePrefab);
		spellProjectileScript = spellProjectile.GetComponent<FireConstantBaseScript> ();

		if (spellProjectileScript == null) {
			// temporary effect, like a fireball
			spellProjectileScript = spellProjectile.GetComponent<FireBaseScript> ();
			if (spellProjectileScript.IsProjectile) {
				// set the start point near the player
				rotation = player.rotation;
				pos = transform.position + forward + up;// + right + up;
			} else {
				// set the start point in front of the player a ways
				pos = transform.position + (forwardY * 10.0f);
			}
		} else {
			// set the start point in front of the player a ways, rotated the same way as the player
			pos = transform.position + (forwardY * 5.0f);
			rotation = player.rotation;
			pos.y = 0.0f;
		}

		FireProjectileScript projectileScript = spellProjectile.GetComponentInChildren<FireProjectileScript> ();
		if (projectileScript != null) {
			// make sure we don't collide with other friendly layers
			projectileScript.ProjectileDestroyParticleSystemsOnCollision [0].startColor = spellColor;
			projectileScript.ProjectileExplosionParticleSystem.startColor = spellColor;
			projectileScript.ProjectileColliderObject.tag = enemyRangeAttackTag;
			projectileScript.ProjectileCollisionLayers &= (~UnityEngine.LayerMask.NameToLayer ("FriendlyLayer"));
		}

		spellProjectile.transform.position = pos;
		spellProjectile.transform.LookAt (player);
	}
}


