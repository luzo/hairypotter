﻿using System;
using UnityEngine;

public class SpellHolder
{

	public static Spell[] spells = new Spell[] {
		new Spell ("Stupefy", "AttackSpellMove", 3f, "#ffff00"),
		new Spell ("Impedimenta", "AttackSpellMove", 15f, "#0000ff"),
		new Spell ("Expeliarmus", "AttackSpellMove", 7f, "#ff3300"),
		new Spell ("AvadaKedavra", "AttackSpellMove", 30f, "#00ff00"),
		new Spell ("Heal", "HealSpellMove", 120f, "#33ccff"),
	};

	public class Spell
	{
		public string spellName;
		public string spellMoveAnimationName;
		public float cooldownTime;
		public Color spellColor;
		public float actualSpellTime = -1f;

		public Spell (string spellName, string spellMoveAnimationName, float cooldownTime, string spellColorHex)
		{
			this.spellName = spellName;
			this.spellMoveAnimationName = spellMoveAnimationName;
			this.cooldownTime = cooldownTime;
			ColorUtility.TryParseHtmlString (spellColorHex, out spellColor);
		}

		public bool isSpellReady ()
		{
			return actualSpellTime == -1f;
		}
	}
}


