﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageController : MonoBehaviour
{
	private static int health;
	private static int maxHealth = 500;
	public Slider slider;
	private bool isDamaged = false;
	public GameObject gameOverCanvas;

	void Start ()
	{
		health = maxHealth;
	}

	void Update ()
	{
		if (health <= 0) {
			gameOverCanvas.SetActive (true);
			Time.timeScale = 0.0f;
			health = 100;
		}

		if (isDamaged) {
			health--;
		}

		slider.value = (int)((float)health * 100 /(float)maxHealth);
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "DangerousWalls") {
			isDamaged = true;
		} else if(other.tag == "Deatheater AvadaKedavra Attack") {
			isDamaged = true;
			reduceHealth (35);
		}
		Debug.Log (other.tag);
	}

	void OnTriggerExit (Collider other)
	{
		isDamaged = false;
	}

	public void heal (int healthPoints)
	{
		health = health + healthPoints < maxHealth ? health + healthPoints : maxHealth;
	}

	public void reduceHealth(int damagePoints){
		health = health - damagePoints;
	}
}
