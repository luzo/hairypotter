﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallDamager : MonoBehaviour {

	public bool isDamaging = false;

	
	// Update is called once per frame
	void Update () {
		if (isDamaging) {
			this.tag = "DangerousWalls";
		} else {
			this.tag = "Walls";
		}
	}
}
