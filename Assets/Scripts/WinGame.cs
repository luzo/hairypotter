﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class WinGame : MonoBehaviour {
	
	void OnTriggerEnter (Collider other)
	{
		switch (other.tag) {
		case "HarryPotter":
			SceneManager.LoadScene ("Outro");
			break;
		default:
			break;
		};
	}
		
}
