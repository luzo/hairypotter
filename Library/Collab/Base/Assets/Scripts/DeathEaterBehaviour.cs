using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathEaterBehaviour : MonoBehaviour
{
	private bool harrySpotted = false;
	public int healthPoints;
	public Transform player;
	public float RotationSpeed = 10f;
	public Transform positionsHolder;

	private List<Transform> teleportPositions;
	private Quaternion _lookRotation;
	private Vector3 _direction;
	private bool wasHit = false;
	private bool isDead = false;
	private Animator animator;

	// Use this for initialization
	void Start ()
	{
		teleportPositions = new List<Transform> ();

		foreach (Transform child in positionsHolder) {
			teleportPositions.Add (child);
		}

		InvokeRepeating ("Teleport", 5f, 15f);

		animator = this.GetComponent <Animator> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		RaycastHit hit;
		bool isBlockedByWall = false;

		if (Physics.Linecast (transform.position, player.transform.position, out hit)) {
			isBlockedByWall = hit.transform.tag == "Walls" || hit.transform.tag == "DangerousWalls";
		}

		if (harrySpotted && !isDead) {
			_direction = (player.position - transform.position).normalized;
			_lookRotation = Quaternion.LookRotation (_direction);
			transform.rotation = Quaternion.Slerp (transform.rotation, _lookRotation, Time.deltaTime * RotationSpeed);
			if (!isBlockedByWall) {
				animator.SetTrigger ("spotted");
			}
		}
	}

	void OnTriggerEnter (Collider other)
	{
		switch (other.tag) {
		case "Stupefy Spell":
			if (!wasHit) {
				ResolveStupefySpellImpact ();
			}
			break;
		case "Expeliarmus Spell":
			if (!wasHit) {
				ResolveExpelliarmusSpellImpact ();
			}
			break;
		case "AvadaKedavra Spell":
			if (!wasHit) {
				ResolveAvadaKedavraSpellImpact ();
			}
			break;
		case "HarryPotter":
			harrySpotted = true;
			break;
		}
	}

	void OnTriggerExit (Collider other)
	{
		if (other.CompareTag ("HarryPotter")) {
			animator.SetTrigger ("free");
			harrySpotted = false;
		}
	}

	private void ResolveStupefySpellImpact ()
	{
		int damage = 10;
		ResolveDamage (damage);
		wasHit = true;
	}

	private void ResolveExpelliarmusSpellImpact ()
	{
		int damage = 20;
		ResolveDamage (damage);
		wasHit = true;
	}

	private void ResolveAvadaKedavraSpellImpact ()
	{
		int damage = 35;
		ResolveDamage (damage);
		wasHit = true;
	}

	private void ResolveDamage (int damage)
	{
		healthPoints -= damage;

		if (healthPoints <= 0) {
			isDead = true;
			animator.SetTrigger ("death");
			Invoke ("Destroy", 8f);
		} 
	}

	private void Destroy ()
	{
		Destroy (this.gameObject);
	}

	private void Teleport ()
	{
		if (!isDead) {
			int randomIndex = Random.Range (0, teleportPositions.Count);
			var choosedPoint = teleportPositions [randomIndex];

			//animator.Stop ();
			animator.SetTrigger ("teleport");
			animator.ResetTrigger ("spotted");
			transform.position = choosedPoint.position;
			transform.LookAt (choosedPoint);
			harrySpotted = false;
		}
	}
}
