﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellHitResolver : MonoBehaviour
{
	public int healthPoints;
	public bool deprecatedAnimations = false;
	public bool isStunned = false;
	public bool isDead = false;
	private bool wasHit = false;

	void Update ()
	{
		if (!deprecatedAnimations) {
			return;
		}
	}

	void OnTriggerEnter (Collider other)
	{
		ResolveSpellImpact (other.tag);
		wasHit = true;
	}

	void OnTriggerExit (Collider other)
	{
		wasHit = false;
	}

	public void ResolveSpellImpact (string spellTag)
	{
		if (!deprecatedAnimations) {
			return;
		}

		if (!isDead && !wasHit) {
			switch (spellTag) {
			case "Stupefy Spell":
				ResolveStupefySpellImpact ();
				break;
			case "Impedimenta Spell":
				ResolveImpedimentaSpellImpact ();
				break;
			case "Expeliarmus Spell":
				ResolveExpelliarmusSpellImpact ();
				break;
			case "AvadaKedavra Spell":
				ResolveAvadaKedavraSpellImpact ();
				break;
			}
		}
	}

	private void ResolveStupefySpellImpact ()
	{
		int damage = 10;
		int freezeTime = 3;
		StartCoroutine (ResolveDamage (damage));
		StartCoroutine (ResolveFreezeTime (freezeTime));
	}

	private void ResolveImpedimentaSpellImpact ()
	{
		int freezeTime = 7;
		StartCoroutine (ResolveFreezeTime (freezeTime));
	}

	private void ResolveExpelliarmusSpellImpact ()
	{
		int damage = 20;
		StartCoroutine (ResolveDamage (damage));
	}

	private void ResolveAvadaKedavraSpellImpact ()
	{
		int damage = 35;
		StartCoroutine (ResolveDamage (damage));
	}

	private IEnumerator ResolveDamage (int damage)
	{
		healthPoints -= damage;
		GetComponent<NavigationAgent> ().agent.enabled = false;
		SpiderAndScorpionAnimationController animator = GetComponent<SpiderAndScorpionAnimationController> ();
		GetComponent<Animation> ().Stop (); 

		animator.isTriggered = true;

		if (healthPoints <= 0) {
			isDead = true;
			GetComponent<Animation> ().Play ("death");
			yield return new WaitForSeconds (10);
			Destroy (this.gameObject);
		} else {
			if (!isStunned) {
		
				GetComponent<Animation> ().Play ("damage");
				yield return new WaitForSeconds (1);
				//animator.isTriggered = false;
				GetComponent<NavigationAgent> ().agent.enabled = true;
			}
		}
	}

	private IEnumerator ResolveFreezeTime (int seconds)
	{
		GetComponent<Animation> ().Stop (); 
		GetComponent<NavigationAgent> ().enabled = false;
		isStunned = true;
		yield return new WaitForSeconds (seconds);
		isStunned = false;
		GetComponent<NavigationAgent> ().enabled = true;
	}
}
