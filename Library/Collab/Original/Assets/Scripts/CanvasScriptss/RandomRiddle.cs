﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomRiddle : MonoBehaviour
{
	public Text text;
	public Dropdown dropdown;
	public GameObject SfingeRiddleCanvas;
	public GameObject spider;
	private Animator wallsAnimator;

	private string[] riddles = new string[] {
		"First think of the person who lives in disguise,\nWho deals in secrets and tells naught but lies.\nNext, tell me what's always the last thing to mend,\nThe middle of middle and end of the end?\nAnd finally give me the sound often heard,\nDuring the search for a hard-to-find word.\nNow string them together and answer me this,\nWhich creature would you be unwilling to kiss?",
		"what do we call person who turns coffee into code?", 
		"What's the object-oriented way to become wealthy?",
		"What is programmer's favourite hangout place?",
		"Who can take a screenshot of his blue screen?"
	};

	private string[,] posibilities = new string[,] { 
		{ "Dog", "Rat", "Cat" },
		{ "Programmer", "Cook", "Barman" },
		{ "Encapsulation", "Polymorphism", "Inheritance" },
		{ "School", "Table", "Foo Bar" },
		{ "Dano Drevo",  "Chuck Norris", "Nobody" }
	};

	private int answer;
	private Vector3 sfingePositon;
	private static int numberOfGeneratedSpiders = 10;
	private int initialHpSpider = 50;
	public GameObject player;
	public Transform navigationHolder;
	public bool deprecatedAnimations;
	private string sfingeName;
	private int[] rightAnswers = new int[]{ 1, 0, 2, 2, 1 };
	private static int numberOfRiddles = 5;
	private Animator _wallsAnimator;
	private GameObject sfinge;

	void Start ()
	{
		regenerateRiddle ();
	}

	public void updateWallsAnimator (Animator wallsAnimator)
	{
		_wallsAnimator = wallsAnimator;
	}

	void Update() {

	}

	private void SetCursor (bool visible)
	{
		Cursor.visible = visible;
		Cursor.lockState = visible ? CursorLockMode.None : CursorLockMode.Locked;
	}

	private int generateRandomNumber (int max)
	{
		return Random.Range (0, max);
	}

	public void validateAnswer ()
	{
		sfinge = GameObject.Find (sfingeName);
		sfingeName = ChooseSfingeOptions.sfingeName;
		sfingePositon = GameObject.Find (sfingeName).transform.position;
		if (dropdown.value == answer) {
			Invoke ("OpenWay", 2f);
			SfingeRiddleCanvas.SetActive (false);
		} else {
			Invoke ("OpenWay", 10f);
			SfingeRiddleCanvas.SetActive (false);
			GenerateSpiders ();
		}

		ChangeSfingeState ();
		Invoke ("ChangeSfingeState",10f);
		SetCursor (false);
		Time.timeScale = 1.0f;
		regenerateRiddle ();
	}

	private void OpenWay() {
		_wallsAnimator.SetTrigger ("riddleCorrect");
	}

	private void GenerateSpiders ()
	{
		for (int i = 0; i < numberOfGeneratedSpiders; i++) {
			GameObject instance = Instantiate (spider, sfingePositon, Quaternion.identity);
			EnemyAnimationController animationCtrl = instance.GetComponent<EnemyAnimationController> ();
			animationCtrl.healthPoints = initialHpSpider;
			animationCtrl.player = player.transform;
			animationCtrl.deprecatedAnimations = deprecatedAnimations;

			NavigationAgent navAgent = instance.GetComponent<NavigationAgent> ();
			navAgent.player = player.transform;
			navAgent.navigationPointsHolder = navigationHolder;
		}
	}

	private void ChangeSfingeState ()
	{		
		
	}

	private void regenerateRiddle ()
	{
		int riddleIndex = generateRandomNumber (numberOfRiddles);
		text.text = riddles [riddleIndex];
		dropdown.ClearOptions ();
		for (int i = 0; i < 3; i++) {
			dropdown.options.Add (new Dropdown.OptionData () { text = posibilities [riddleIndex, i] });
		}
		dropdown.RefreshShownValue ();
		dropdown.value = 0;
		answer = rightAnswers [riddleIndex];
	}
}
